// Grab all list item headers
const listItemHeaders = document.querySelectorAll('.js-itemHeader');

// Loop headers
listItemHeaders.forEach(listItemHeader => {
  listItemHeader.addEventListener('click', event => {

    // Optional -> Comment the next 5 lines to allow all lists to be open at the same time.
    const activelistItemHeader = document.querySelector('.js-itemHeader.active');

    if(activelistItemHeader && activelistItemHeader !== listItemHeader) {
      activelistItemHeader.classList.toggle('active');
      activelistItemHeader.nextElementSibling.style.maxHeight = 0;
    }

    // Toggle 'active' class on the list-item.
    listItemHeader.classList.toggle('active');

    // Grab the 'active' list-item content
    const listItemContent = listItemHeader.nextElementSibling;

    if(listItemHeader.classList.contains('active')) {
      listItemContent.style.maxHeight = listItemContent.scrollHeight + 'px';
    } else {
      listItemContent.style.maxHeight = 0;
    }
  });
});