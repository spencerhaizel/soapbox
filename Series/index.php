<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Series</title>

    <link rel="stylesheet" href="css/styles.css" type="text/css">
</head>

<?php
    $series = [
        [
            "id" => "0",
            "title" => "Think Tanks and Universities: a whole greater than the sum of the parts?",
            "name"   => "Peter Taylor",
            "date" => "June 17, 2015",
            "content" => "This is the first post from a series on Think Tanks and Universities. In this post, Peter Taylor reflects on the challenges of selecting beneficiaries that apply for funding, which can include both think tanks and universities. <br><br> As collaboration becomes an increasingly important element of knowledge generation, donors have to be careful that the relationship between think tanks and universities is not affected by competition for funds.",
        ],
        [
            "id" => "1",
            "title" => "Más Saber América Latina: Promoting links between think tanks and universities",
            "name"   => "Adriana Arellano, Orazio Bellettini",
            "date" => "June 18, 2015",
            "content" => "Senectus et netus et malesuada fames ac. Tincidunt lobortis feugiat vivamus at. Bibendum ut tristique et egestas quis ipsum suspendisse. Nibh tortor id aliquet lectus proin nibh. Viverra justo nec ultrices dui sapien. Accumsan sit amet nulla facilisi. <br><br> Quis commodo odio aenean sed adipiscing diam donec adipiscing tristique. Sociis natoque penatibus et magnis dis parturient montes.",
        ],
        [
            "id" => "2",
            "title" => "Universities and Think Tanks in Africa: Competing or Complementing?",
            "name"   => "Darlison Kaija",
            "date" => "June 26, 2015",
            "content" => "Habitant morbi tristique senectus et netus et malesuada fames ac. Turpis nunc eget lorem dolor sed viverra ipsum nunc aliquet. Interdum varius sit amet mattis vulputate enim. <br><br> Urna condimentum mattis pellentesque id nibh tortor id aliquet. Nunc sed velit dignissim sodales. Sit amet volutpat consequat mauris nunc congue nisi vitae suscipit. Laoreet suspendisse interdum consectetur libero id.",
        ],
        [
            "id" => "3",
            "title" => "Think tanks and universities - Different, vet similar: The South Asian Context",
            "name"   => "Arif Naveed",
            "date" => "July 1, 2015",
            "content" => "Dui id ornare arcu odio. Vestibulum sed arcu non odio euismod lacinia at quis. Diam quis enim lobortis scelerisque fermentum dui faucibus in. Bibendum ut tristique et egestas quis ipsum suspendisse ultrices. Odio morbi quis commodo odio. Nisl rhoncus mattis rhoncus urna neque viverra justo nec ultrices. Laoreet non curabitur gravida arcu. <br><br> Mi proin sed libero enim sed faucibus turpis in eu. Interdum consectetur libero id faucibus nisl. Ultricies mi eget mauris pharetra et ultrices. Amet justo donec enim diam vulputate ut pharetra sit amet. Lorem sed risus ultricies tristique nulla aliquet enim tortor at.",
        ],
        [
            "id" => "4",
            "title" => "The Think Tanks and University series - Next steps: lumping the hurdles",
            "name"   => "Shannon Sutton",
            "date" => "July 8, 2015",
            "content" => "Turpis egestas sed tempus urna et pharetra pharetra massa massa. Orci sagittis eu volutpat odio facilisis mauris. A iaculis at erat pellentesque adipiscing commodo elit at. <br><br> Sed risus pretium quam vulputate dignissim suspendisse in. Eget gravida cum sociis natoque. Cursus metus aliquam eleifend mi in nulla. Urna porttitor rhoncus dolor purus non enim praesent elementum facilisis. Eleifend donec pretium vulputate sapien nec sagittis aliquam. Iaculis eu non diam phasellus. <br><br> Vitae turpis massa sed elementum tempus egestas sed. Molestie a iaculis at erat. Curabitur vitae nunc sed velit dignissim sodales ut eu. Gravida dictum fusce ut placerat orci nulla pellentesque.",
        ]
    ];
?>

<body>
    <header class="header">
        <div class="container">
            <span class="header__tab header__tab--dark">Series</span>

            <h1>Think Tanks and Universities</h1>
            <p>This is a series on the relationship between Think Tanks and Universities, edited by <a href="#" target="_blank">Shannon Sutton</a>, from The Think Tank Initiative. The reports from the accompanying study can be found on the <a href="#" target="_blank">Think Tank Initiative's website</a>. The posts in the series relate to research undertaken in Latin America. Africa and South Asia. For further information visit the Theme Page on <a href="#" target="_blank">Think Tanks and Universities</a>.</p>
        </div>
    </header>

    <main class="content">
        <ul class="list" role="list">
            <?php foreach($series as $data) : ?>
                <li class="list-item">
                    <div class="container list-item__header js-itemHeader">
                        <h2 class="list-item__title"><?php echo $data["title"]; ?></h2>

                        <div class="list-item__meta">
                            <span class="list-item__author">By <strong><?php echo $data["name"]; ?></strong></span>
                            <date class="list-item__date"><?php echo $data["date"]; ?></date>
                        </div>
                    </div>

                    <div class="list-item__content js-itemContent">
                        <div class="container list-item__inside">
                            <p><?php echo $data["content"]; ?></p>

                            <a class="uppercase" href="#" target="_blank">Read Full Article</a>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </main>

    <script type="text/javascript" src='js/scripts.js' id='js-scripts'></script>
</body>
</html>