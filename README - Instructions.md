# HTML Accordion

### Task
* Using the documents provided, reproduce the given designs using HTML, PHP, CSS and Javascript. 
* Beneath the title and standfirst are five listed items which should work as accordions.
* The accordion should use PHP to loop through an array of content and output five accordion items.
* Please use this as an opportunity to show off your attention to detail, accessibility considerations, how you structure your code, etc. 


### Assets
* Design of the page, its hover state and its open state
* A spec sheet with measurements for the page and its open state
* A type spec sheet

(Don't worry, normally our designers deliver stuff to us via Zeplin or Figma).


### Notes
* The fonts used within the design are commercial, and therefore not available to use for this purpose. Please replace references to ‘Circe’ with an appropriate sans-serif font of your choice and references to ‘Leitura’ with an appropriate serif of your choice
* The text within each accordion item can just be lorem ipsum or any dummy text
* Do not use any CSS frameworks although pre/post processors are welcome e.g. SCSS, PostCSS.
* Task runners/automation are welcome
* Feel free to use jQuery, but please do not use a third-party plugin


### Completion
* Once you’ve completed this task, please zip the files, add your name to the name of the zip and send them to graham.may@soapbox.co.uk


### Questions
* If you have any questions, please contact Graham: graham.may@soapbox.co.uk.